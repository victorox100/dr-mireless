interface ILocation {
  id: Number;
  name: String;
  shortName: String;
  address: String;
  office_number: Number;
  phone_number: String;
  city: String;
  state: String;
  postalCode: String;
}
export default ILocation;
