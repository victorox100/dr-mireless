export default interface IMedicalConsultation {
  id?: Number;
  user: Number;
  physical_exploration: String;
  study_result?: File | String;
  study_description: String;
  diagnosis: String;
  observations: String;
  date: Date;
  owner?: Number;
}
