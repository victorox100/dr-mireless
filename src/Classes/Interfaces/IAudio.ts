interface Audio {
  id: Number;
  name: String;
  audio: File | string | null;
  order: Number;
  duration: Number;
}
export default Audio;
