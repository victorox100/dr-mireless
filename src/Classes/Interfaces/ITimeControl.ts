export default interface ITimeControl {
  id: Number;
  initial_timetable: String;
  final_timetable: String;
  validity: Number;
  term: Number;
}
