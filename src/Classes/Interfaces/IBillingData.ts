interface IBillingData {
  id?: Number;
  user?: Number;
  BusinessName: String;
  rfc: String;
  email: String;
  address: String;
  city: String;
  state: String;
  cp: String;
}
export default IBillingData;
