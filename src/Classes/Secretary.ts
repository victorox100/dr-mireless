import User from "./User";
import Service from "./Service";
import ICustomer from "./Interfaces/ICustomer";
import jwt from "jsonwebtoken";
import IBillingData from "./Interfaces/IBillingData";
import IAppointment from "./Interfaces/IAppointment";
import { View } from "@fullcalendar/core";
import IClinicalHistory from "./Interfaces/IClinicalHistory";
import IPayment from "./Interfaces/IPayment";
import INotification from "./Interfaces/INotification";
class Secretary extends User {
  addCustomer = async (
    customer: ICustomer & { password: String; password_confirm: String },
    billigData: IBillingData
  ) => {
    const service: Service = new Service({});
    const response = await service.post("register/", { ...customer, type: 3 });
    if (response.error) {
      return false;
    } else {
      const decoded: any = jwt.decode(response.access);
      const userResponse = await service.get(`users/${decoded.user_id}`);
      if (userResponse.error) {
        return false;
      } else {
        const assign_response = await service.post(
          "v1/api/create-assign-branch-office/",
          { user: decoded.user_id, branch_office: this.location_id }
        );
        if (assign_response.id) {
          if (await this.createBilling(decoded.user_id, billigData)) {
            return userResponse;
          } else {
            return false;
          }
        } else {
          return false;
        }
      }
    }
  };
  getCustomers = async (page: Number = 1) => {
    await this.setLocation();
    const service: Service = new Service({});
    const response = await service.get(
      `v1/api/assign-branch-office/?branch_office=${this.location_id}&page=${page}&type=3`
    );
    if (response.error) {
      return false;
    } else {
      return {
        data: response.data,
        pagination: {
          first: 1,
          last: response.pagination.links.last
            .replace(
              process.env.REACT_APP_API_URL +
                `v1/api/assign-branch-office/?branch_office=${this.location_id}&page=`,
              ""
            )
            .replace("&type=3", ""),
          next: response.pagination.links.next
            ? response.pagination.links.next
                .replace(
                  process.env.REACT_APP_API_URL +
                    `v1/api/assign-branch-office/?branch_office=${this.location_id}&page=`,
                  ""
                )
                .replace("&type=3", "")
            : null,
          prev: response.pagination.links.prev
            ? response.pagination.links.prev
                .replace(
                  process.env.REACT_APP_API_URL +
                    `v1/api/assign-branch-office/?branch_office=${this.location_id}&page=`,
                  ""
                )
                .replace("&type=3", "")
            : null,
        },
      };
    }
  };
  updateCustomer = async (
    customer: ICustomer
  ): Promise<ICustomer | Boolean> => {
    const service: Service = new Service({});
    const response = await service.put(`users/${customer.id}/`, customer);
    return response.error ? false : response;
  };
  getCustomer = async (id: Number): Promise<ICustomer | Boolean> => {
    const service: Service = new Service({});
    const customer = await service.get(`users/${id}`);
    return customer.error ? false : customer;
  };
  createBilling = async (
    id: Number,
    billingData: IBillingData
  ): Promise<IBillingData | Boolean> => {
    const service: Service = new Service({});
    const billingDataResponse = await service.post("v1/api/create-billing/", {
      user: id,
      ...billingData,
    });
    return billingDataResponse.error ? false : billingDataResponse;
  };
  getBillingByUser = async (id: Number): Promise<IBillingData | Boolean> => {
    const service: Service = new Service({});
    const billing = await service.get(`v1/api/billing/?user=${id}`);
    return billing.error
      ? false
      : billing.data.length > 0
      ? billing.data[0]
      : false;
  };
  updateBilling = async (
    billingData: IBillingData
  ): Promise<IBillingData | Boolean> => {
    const service: Service = new Service({});
    const response = await service.put(
      `v1/api/billing/${billingData.id}/`,
      billingData
    );
    return response.error ? false : response;
  };
  getTerapists = async (page: Number = 1) => {
    await this.setLocation();
    const service: Service = new Service({});
    const response = await service.get(
      `v1/api/assign-branch-office/?branch_office=${this.location_id}&page=${page}&type=4`
    );
    if (response.error) {
      return false;
    } else {
      return {
        data: response.data,
        pagination: {
          first: 1,
          last: response.pagination.links.last
            ? response.pagination.links.last
                .replace(
                  process.env.REACT_APP_API_URL +
                    `v1/api/assign-branch-office/?branch_office=${this.location_id}&page=`,
                  ""
                )
                .replace("&type=4", "")
            : null,
          next: response.pagination.links.next
            ? response.pagination.links.next
                .replace(
                  process.env.REACT_APP_API_URL +
                    `v1/api/assign-branch-office/?branch_office=${this.location_id}&page=`,
                  ""
                )
                .replace("&type=4", "")
            : null,
          prev: response.pagination.links.prev
            ? response.pagination.links.prev
                .replace(
                  process.env.REACT_APP_API_URL +
                    `v1/api/assign-branch-office/?branch_office=${this.location_id}&page=`,
                  ""
                )
                .replace("&type=4", "")
            : null,
        },
      };
    }
  };
  /**
   * Appointment
   */
  scheduleAppointment = async (appointment: IAppointment) => {
    if (this.location_id === undefined) {
      await this.setLocation();
    }
    appointment.branchOffice = this.location_id as Number;
    const service: Service = new Service({});
    const response = await service.post("v1/api/appointment/", appointment);
    return response.error ? false : response;
  };

  getAppointments = async (
    view: View,
    therapist?: Number,
    status?: Number
  ): Promise<IAppointment[] | Boolean> => {
    if (this.location_id === undefined) {
      await this.setLocation();
    }
    const service: Service = new Service({});
    let query: String = `v1/api/appointment/?branchOffice=${this.location_id}`;
    switch (view.type) {
      case "dayGridMonth":
      case "timeGridWeek":
        query += `&start_range=${view.currentStart
          .toISOString()
          .substr(0, 10)}&end_range=${view.currentEnd
          .toISOString()
          .substr(0, 10)}`;
        break;
      case "timeGridDay":
        query += `&date=${view.currentStart.toISOString().substr(0, 10)}`;
        break;
    }
    if (therapist !== undefined) {
      query += `&therapist=${therapist}`;
    }
    if (status !== undefined) {
      query += `&status=${status}`;
    }
    let response = await service.get(query);
    if (!response.error) {
      response = await service.get(
        query + "&per_page=" + response.pagination.total_rows
      );
    } else {
      return false;
    }
    return response.error ? false : response.data;
  };

  getAppointment = async (id: String) => {
    const service: Service = new Service({});
    const response = await service.get(`v1/api/appointment/${id}`);
    return response.error ? false : response;
  };

  updateAppointment = async (
    event: IAppointment
  ): Promise<IAppointment | Boolean> => {
    const service: Service = new Service({});
    const response = await service.put(
      `v1/api/appointment/${event.id}/`,
      event
    );
    return response.error ? false : response;
  };

  /**
   * Medical history
   */

  createClinicalHistory = async (
    history: IClinicalHistory
  ): Promise<IClinicalHistory | Boolean> => {
    const service: Service = new Service({});
    const response = await service.post(`v1/api/medical-history/`, history);
    return response.error ? false : response;
  };

  getClinicalHistory = async (
    user: Number
  ): Promise<IClinicalHistory[] | Boolean> => {
    const service: Service = new Service({});
    const response = await service.get(`v1/api/medical-history/?user=${user}`);
    return response.error ? false : response.data;
  };

  updateClinicalHistory = async (
    history: IClinicalHistory
  ): Promise<IClinicalHistory | Boolean> => {
    const service: Service = new Service({});
    const response = await service.put(
      `v1/api/medical-history/${history.id}/`,
      history
    );
    return response.error ? false : response;
  };

  /**
   * Payment
   */

  createPayment = async (payment: IPayment): Promise<Boolean> => {
    if (this.location_id === undefined) {
      await this.setLocation();
    }
    const service = new Service({});
    payment.branchOffice = this.location_id as Number;
    const response = await service.post("v1/api/payment/", payment);
    return response.error ? false : true;
  };

  sendByEmail = async (formData: FormData): Promise<Boolean> => {
    const service: Service = new Service({});
    const response = await service.post(`v1/api/receipt/`, formData);
    return response.error ? false : true;
  };

  getPayments = async (
    page: Number,
    start_range?: String,
    end_range?: String,
    user?: Number
  ): Promise<{ data: IPayment[]; pagination: any } | Boolean> => {
    if (this.location_id === undefined) {
      await this.setLocation();
    }
    let query: String = `v1/api/payment/?branchOffice=${this.location_id}&page=${page}`;
    if (start_range) {
      query += `&start_range=${start_range}`;
    }
    if (end_range) {
      query += `&end_range=${end_range}`;
    }
    if (user != undefined) {
      query += `&user=${user}`;
    }
    const service: Service = new Service({});
    const response = await service.get(query);
    if (response.error) return false;
    response.data.payments = await Promise.all(
      response.data.payments.map(async (payment: IPayment) => {
        const user = await this.getCustomer(payment.user as Number);
        const branch = await this.getLocation(payment.branchOffice as Number);
        return {
          ...payment,
          user: `${(user as ICustomer).first_name} ${
            (user as ICustomer).last_name
          }`,
          branchOffice: branch.shortName,
        };
      })
    );
    return response.error
      ? false
      : {
          data: response.data.payments,
          pagination: {
            first: 1,
            last: response.pagination.links.last
              ? response.pagination.links.last.match(/page=([\d]+)/m)[1]
              : null,
            next: response.pagination.links.next
              ? response.pagination.links.next.match(/page=([\d]+)/m)[1]
              : null,
            prev: response.pagination.links.prev
              ? response.pagination.links.prev.match(/page=([\d]+)/m)[1]
              : null,
          },
        };
  };

  /**
   * Treatments
   *
   */

  getTreatments = async (id: Number, page: Number = 1) => {
    const service: Service = new Service({});
    const response = await service.get(
      `v1/api/treatment/?users=${id}&page=${page}`
    );
    return response.error
      ? false
      : {
          data: response.data,
          pagination: {
            first: 1,
            last: response.pagination.links.last
              ? response.pagination.links.last.replace(
                  process.env.REACT_APP_API_URL +
                    `v1/api/treatment/?users=${id}&page=`,
                  ""
                )
              : null,
            next: response.pagination.links.next
              ? response.pagination.links.next.replace(
                  process.env.REACT_APP_API_URL +
                    `v1/api/treatment/?users=${id}&page=`,
                  ""
                )
              : null,
            prev: response.pagination.links.prev
              ? response.pagination.links.prev.replace(
                  process.env.REACT_APP_API_URL +
                    `v1/api/treatment/?users=${id}&page=`,
                  ""
                )
              : null,
          },
        };
  };

  /**
   * Notifications
   */

  addNotification = async (notification: INotification) => {
    const service = new Service({});
    const response = await service.post(
      "v1/api/notification-register/",
      notification
    );
    return response.error ? false : response;
  };

  getAllNotifications = async (
    page: Number = 1
  ): Promise<
    | Boolean
    | {
        data: any;
        pagination: { first: Number; last: Number; prev: Number; next: Number };
      }
  > => {
    const service: Service = new Service({});
    const response = await service.get(`v1/api/notification/?page=${page}`);
    return response.error
      ? false
      : {
          data: response.data,
          pagination: {
            first: 1,
            last: response.pagination.links.last
              ? response.pagination.links.last.replace(
                  process.env.REACT_APP_API_URL + `v1/api/notification/?page=`,
                  ""
                )
              : null,
            next: response.pagination.links.next
              ? response.pagination.links.next.replace(
                  process.env.REACT_APP_API_URL + `v1/api/notification/?page=`,
                  ""
                )
              : null,
            prev: response.pagination.links.prev
              ? response.pagination.links.prev.replace(
                  process.env.REACT_APP_API_URL + `v1/api/notification/?page=`,
                  ""
                )
              : null,
          },
        };
  };

  /**
   * Disease
   */

  getDiseases = async (page: Number = 1) => {
    const service: Service = new Service({});
    const response = await service.get("v1/api/disease/?page=" + page);
    if (response.error) {
      return false;
    } else {
      const pages = response.pagination.links;
      return {
        data: response.data,
        pagination: {
          prev: pages.prev
            ? pages.prev.replace(
                process.env.REACT_APP_API_URL + "v1/api/disease/?page=",
                ""
              )
            : null,
          next: pages.next
            ? pages.next.replace(
                process.env.REACT_APP_API_URL + "v1/api/disease/?page=",
                ""
              )
            : null,
          last: pages.last
            ? pages.last.replace(
                process.env.REACT_APP_API_URL + "v1/api/disease/?page=",
                ""
              )
            : null,
          first: pages.first
            ? pages.first.replace(
                process.env.REACT_APP_API_URL + "v1/api/disease/?page=",
                ""
              )
            : null,
        },
      };
    }
  };
  getMedicalConsultations = async (
    page: Number = 1,
    start_range?: String,
    end_range?: String,
    user?: Number,
    therapist?: Number
  ) => {
    const service: Service = new Service({});
    const response = await service.get(
      `v1/api/medical-consultation/?owner=${therapist || ""}&user=${
        user || ""
      }&start_range=${start_range}&end_range=${end_range}&page=${page}&branchOffice=${
        this.location_id
      }`
    );
    return response.error
      ? false
      : {
          data: response.data,
          pagination: {
            first: 1,
            last: response.pagination.links.last
              ? response.pagination.links.last.match(/page=([\d]+)/m)[1]
              : null,
            next: response.pagination.links.next
              ? response.pagination.links.next.match(/page=([\d]+)/m)[1]
              : null,
            prev: response.pagination.links.prev
              ? response.pagination.links.prev.match(/page=([\d]+)/m)[1]
              : null,
          },
        };
  };
}

export default Secretary;
