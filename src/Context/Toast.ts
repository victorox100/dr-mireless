import React from "react";
import { AlertMessage } from "../Components/ActionModals/Toast";
interface ToastContext {
  alerts: AlertMessage[];
  setAlert: Function;
}
const Context: ToastContext = {
  alerts: [],
  setAlert: () => {},
};
const ToastContext = React.createContext<ToastContext>(Context);
export default ToastContext;
