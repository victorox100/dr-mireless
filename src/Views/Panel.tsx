import React, { useContext, Suspense } from "react";
import {
  withRouter,
  Switch,
  Route,
  RouteComponentProps,
  useLocation,
  Redirect,
} from "react-router-dom";
import clsx from "clsx";
import {
  createStyles,
  makeStyles,
  useTheme,
  Theme,
} from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import List from "@material-ui/core/List";
import CssBaseline from "@material-ui/core/CssBaseline";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import PlaceIcon from "@material-ui/icons/Place";
import UserContext from "../Context/User";
import SupervisedUserCircleIcon from "@material-ui/icons/SupervisedUserCircle";
import PatientIcon from "@material-ui/icons/AirlineSeatFlatAngled";
import AudioIcon from "@material-ui/icons/Audiotrack";
import Secretary from "../Classes/Secretary";
import Staff from "../Classes/Staff";
import SuperAdmin from "../Classes/SuperAdmin";
import Disease from "./Layout/Disease";
import DiseaseIcon from "@material-ui/icons/BugReport";
import EventIcon from "@material-ui/icons/Event";
import Grid from "@material-ui/core/Grid";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import AssignmentIcon from "@material-ui/icons/Assignment";
import Terapist from "../Classes/Terapist";
import AttachMoneyIcon from "@material-ui/icons/AttachMoney";
import AddAlertIcon from "@material-ui/icons/AddAlert";
import AccessTimeIcon from "@material-ui/icons/AccessTime";
import AdminLocation from "../Components/Buttons/AdminLocation";
import Loader from "../Components/Loader/Loader";

const drawerWidth = 240;
const Location = React.lazy(() => import("./Layout/Location"));
const Audio = React.lazy(() => import("./Layout/Audio"));
const RegisterUsers = React.lazy(() => import("./Layout/RegisterUsers"));
const RegisterCustomer = React.lazy(() => import("./Layout/RegisterCustomer"));
const Appointment = React.lazy(() => import("./Layout/Appointment"));
const ClinicalConsulting = React.lazy(() =>
  import("./Layout/ClinicalConsulting")
);
const MyAppointments = React.lazy(() => import("./Layout/MyAppointments"));
const MyConsults = React.lazy(() => import("./Layout/MyConsults"));
const PaymentReceipt = React.lazy(() => import("./Layout/PaymentReceipt"));
const Payments = React.lazy(() => import("./Layout/Payments"));
const TherapyAssigment = React.lazy(() => import("./Layout/TherapyAssignment"));
const PushNotification = React.lazy(() => import("./Layout/PushNotification"));
const Home = React.lazy(() => import("../Components/Background/BranchBg"));
const TimeInterval = React.lazy(() => import("./Layout/TimeInterval"));

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: "flex",
    },
    appBar: {
      zIndex: theme.zIndex.drawer + 1,
      transition: theme.transitions.create(["width", "margin"], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      backgroundColor: "var(--principal)",
    },
    appBarShift: {
      marginLeft: drawerWidth,
      width: `calc(100% - ${drawerWidth}px)`,
      transition: theme.transitions.create(["width", "margin"], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    menuButton: {
      marginRight: 36,
    },
    hide: {
      display: "none",
    },
    drawer: {
      width: drawerWidth,
      flexShrink: 0,
      whiteSpace: "nowrap",
    },
    drawerOpen: {
      width: drawerWidth,
      transition: theme.transitions.create("width", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    drawerClose: {
      transition: theme.transitions.create("width", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      overflowX: "hidden",
      width: theme.spacing(7) + 1,
      [theme.breakpoints.up("sm")]: {
        width: theme.spacing(9) + 1,
      },
    },
    toolbar: {
      display: "flex",
      alignItems: "center",
      justifyContent: "flex-end",
      padding: theme.spacing(0, 1),
      // necessary for content to be below app bar
      ...theme.mixins.toolbar,
    },
    content: {
      flexGrow: 1,
      padding: theme.spacing(3),
    },
  })
);

function Panel(props: RouteComponentProps) {
  const currentLocation = useLocation();
  const [open, setOpen] = React.useState(true);
  const context = useContext<UserContext>(UserContext);
  const classes = useStyles();
  const theme = useTheme();
  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  const getAuthorizedNavigation = () => {
    let authorizedNavigation;
    switch (true) {
      case context.user instanceof SuperAdmin:
        authorizedNavigation = [
          { text: "Sucursales", icon: <PlaceIcon />, link: "/sucursales" },
          {
            text: "Usuarios",
            icon: <SupervisedUserCircleIcon />,
            link: "/usuarios",
          },
          { text: "Pacientes", icon: <PatientIcon />, link: "/pacientes" },
          { text: "Citas", icon: <EventIcon />, link: "/citas" },
          {
            text: "Biofrecuencias",
            icon: <AudioIcon />,
            link: "/biofrecuencias",
          },
          {
            text: "Enfermedades",
            icon: <DiseaseIcon />,
            link: "/enfermedades",
          },
          { text: "Pagos", icon: <AttachMoneyIcon />, link: "/pagos" },
          {
            text: "Notificaciones",
            icon: <AddAlertIcon />,
            link: "/notificaciones",
          },
          {
            text: "Control de tiempo",
            icon: <AccessTimeIcon />,
            link: "/control",
          },
          {
            text: "Consultas",
            icon: <AssignmentIcon />,
            link: "/consultas",
          },
        ];
        break;
      case context.user instanceof Secretary:
        authorizedNavigation = [
          { text: "Pacientes", icon: <PatientIcon />, link: "/pacientes" },
          { text: "Citas", icon: <EventIcon />, link: "/citas" },
          { text: "Pagos", icon: <AttachMoneyIcon />, link: "/pagos" },
          {
            text: "Notificaciones",
            icon: <AddAlertIcon />,
            link: "/notificaciones",
          },
          {
            text: "Consultas",
            icon: <AssignmentIcon />,
            link: "/consultas",
          },
        ];
        break;
      case context.user instanceof Staff:
        authorizedNavigation = [
          {
            text: "Biofrecuencias",
            icon: <AudioIcon />,
            link: "/biofrecuencias",
          },
          {
            text: "Enfermedades",
            icon: <DiseaseIcon />,
            link: "/enfermedades",
          },
        ];
        break;
      case context.user instanceof Terapist:
        authorizedNavigation = [
          { text: "Mis citas", icon: <EventIcon />, link: "/citas" },
          {
            text: "Mis consultas",
            icon: <AssignmentIcon />,
            link: "/consultas",
          },
        ];
        break;
      default:
        authorizedNavigation = [
          { text: "Invitado", icon: <PlaceIcon />, link: "/Invitado" },
        ];
    }
    return authorizedNavigation.map((menuItem, index) => (
      <ListItem
        button
        key={index}
        onClick={() => {
          props.history.push(menuItem.link as string);
        }}
        style={{
          backgroundColor: new RegExp(menuItem.link).test(
            currentLocation.pathname
          )
            ? "var(--secondary)"
            : "white",
        }}
      >
        <ListItemIcon
          style={{
            color: new RegExp(menuItem.link).test(currentLocation.pathname)
              ? "white"
              : "var(--secondary)",
          }}
        >
          {menuItem.icon}
        </ListItemIcon>
        <ListItemText
          style={{
            fontWeight: "bold",
            color: new RegExp(menuItem.link).test(currentLocation.pathname)
              ? "white"
              : "var(--secondary)",
          }}
          primary={menuItem.text}
        />
      </ListItem>
    ));
  };
  const getAuthorizedContent = () => {
    let content: any[] = [];
    switch (true) {
      case context.user instanceof SuperAdmin:
        content = [
          <Route key="branch" exact path="/sucursales">
            <Suspense fallback={<Loader />}>
              <Location />
            </Suspense>
          </Route>,
          <Route key="users" exact path="/usuarios">
            <Suspense fallback={<Loader />}>
              <RegisterUsers />
            </Suspense>
          </Route>,
          <Route key="audios" exact path="/biofrecuencias">
            <Suspense fallback={<Loader />}>
              <Audio />
            </Suspense>
          </Route>,
          <Route key="disease" exact path="/enfermedades">
            <Suspense fallback={<Loader />}>
              <Disease />
            </Suspense>
          </Route>,
          <Route key="appointment" exact path="/citas/:id?">
            <Suspense fallback={<Loader />}>
              <Appointment />
            </Suspense>
          </Route>,
          <Route key="payments" exact path="/pagos">
            <Suspense fallback={<Loader />}>
              <Payments />
            </Suspense>
          </Route>,
          <Route key="paymentregister" exact path="/registrar-pagos/:patient?">
            <Suspense fallback={<Loader />}>
              <PaymentReceipt />
            </Suspense>
          </Route>,
          <Route key="therapies" exact path="/terapia/:id">
            <Suspense fallback={<Loader />}>
              <TherapyAssigment />
            </Suspense>
          </Route>,
          <Route
            key="notifications"
            exact
            path="/notificaciones/:notification_type?/:id?"
          >
            <Suspense fallback={<Loader />}>
              <PushNotification />
            </Suspense>
          </Route>,
          <Route key="control" exact path="/control">
            <Suspense fallback={<Loader />}>
              <TimeInterval />
            </Suspense>
          </Route>,
          <Route key="customers" exact path="/pacientes">
            <Suspense fallback={<Loader />}>
              <RegisterCustomer />
            </Suspense>
          </Route>,
          <Route key="appointment" exact path="/citas/:id?">
            <Suspense fallback={<Loader />}>
              <Appointment />
            </Suspense>
          </Route>,
          <Route key="myconsults" exact path="/consultas">
            <Suspense fallback={<Loader />}>
              <MyConsults />
            </Suspense>
          </Route>,
        ];
        break;
      case context.user instanceof Secretary:
        content = [
          <Route key="customers" exact path="/pacientes">
            <Suspense fallback={<Loader />}>
              <RegisterCustomer />
            </Suspense>
          </Route>,
          <Route key="appointment" exact path="/citas/:id?">
            <Suspense fallback={<Loader />}>
              <Appointment />
            </Suspense>
          </Route>,
          <Route key="paymentregister" exact path="/registrar-pagos/:patient?">
            <Suspense fallback={<Loader />}>
              <PaymentReceipt />
            </Suspense>
          </Route>,
          <Route key="payments" exact path="/pagos">
            <Suspense fallback={<Loader />}>
              <Payments />
            </Suspense>
          </Route>,
          <Route
            key="notifications"
            exact
            path="/notificaciones/:notification_type?/:id?"
          >
            <Suspense fallback={<Loader />}>
              <PushNotification />
            </Suspense>
          </Route>,
          <Route key="myconsults" exact path="/consultas">
            <Suspense fallback={<Loader />}>
              <MyConsults />
            </Suspense>
          </Route>,
        ];
        break;
      case context.user instanceof Staff:
        content = [
          <Route key="audios" exact path="/biofrecuencias">
            <Suspense fallback={<Loader />}>
              <Audio />
            </Suspense>
          </Route>,
          <Route key="disease" exact path="/enfermedades">
            <Suspense fallback={<Loader />}>
              <Disease />
            </Suspense>
          </Route>,
          <Route key="therapies" exact path="/terapia/:id">
            <Suspense fallback={<Loader />}>
              <TherapyAssigment />
            </Suspense>
          </Route>,
        ];
        break;
      case context.user instanceof Terapist:
        content = [
          <Route key="myappointments" exact path="/citas">
            <Suspense fallback={<Loader />}>
              <MyAppointments />
            </Suspense>
          </Route>,
          <Route key="consulting" exact path="/consulta/:patient">
            <Suspense fallback={<Loader />}>
              <ClinicalConsulting />
            </Suspense>
          </Route>,
          <Route key="myconsults" exact path="/consultas">
            <Suspense fallback={<Loader />}>
              <MyConsults />
            </Suspense>
          </Route>,
        ];
        break;
    }
    content.push(
      <Route key="home" exact path="/">
        <Suspense fallback={<Loader />}>
          <Home />
        </Suspense>
      </Route>
    );
    content.push(<Redirect to="/" />);
    return content;
  };
  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar
        position="fixed"
        className={clsx(classes.appBar, {
          [classes.appBarShift]: open,
        })}
      >
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="start"
            className={clsx(classes.menuButton, {
              [classes.hide]: open,
            })}
          >
            <MenuIcon />
          </IconButton>
          <Grid container direction="row" justify="space-between">
            <Grid
              item
              sm={6}
              xs={12}
              alignItems="center"
              container
              direction="row"
            >
              <img
                alt="logo"
                style={{
                  height: 64,
                  width: "auto",
                  marginRight: "1rem",
                }}
                src={require("../assets/images/logo.png")}
              />
              <Typography
                style={{ fontFamily: "DaggerSquare" }}
                variant="h6"
                noWrap
              >
                DR MIRELES BIOFRECUENCIAS
              </Typography>
            </Grid>
            {context.user instanceof SuperAdmin ? (
              <Grid item sm={3} xs={12} container alignItems="center">
                <AdminLocation />
              </Grid>
            ) : (
              <Grid
                item
                sm={3}
                xs={12}
                container
                justify="flex-end"
                alignItems="center"
              >
                <Typography style={{ fontSize: "0.895rem" }} variant="body1">
                  {(context.user as any).location_name}
                </Typography>
              </Grid>
            )}
            <Grid
              onClick={() => {
                context.setUser(null);
                localStorage.clear();
              }}
              item
              alignItems="center"
              container
              justify="flex-end"
              sm={3}
              xs={12}
              style={{ cursor: "pointer" }}
            >
              <Typography
                variant="body1"
                style={{ marginRight: "0.5rem", fontSize: "0.895rem" }}
              >{`${
                (context.user as SuperAdmin | Staff | Secretary | Terapist)
                  .first_name
              } ${
                (context.user as SuperAdmin | Staff | Secretary | Terapist)
                  .last_name
              }`}</Typography>
              <ExitToAppIcon />
              <Typography style={{ fontSize: "0.895rem" }} variant="body1">
                Salir
              </Typography>
            </Grid>
          </Grid>
        </Toolbar>
      </AppBar>
      <Drawer
        variant="permanent"
        className={clsx(classes.drawer, {
          [classes.drawerOpen]: open,
          [classes.drawerClose]: !open,
        })}
        classes={{
          paper: clsx({
            [classes.drawerOpen]: open,
            [classes.drawerClose]: !open,
          }),
        }}
      >
        <div className={classes.toolbar}>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === "rtl" ? (
              <ChevronRightIcon />
            ) : (
              <ChevronLeftIcon />
            )}
          </IconButton>
        </div>
        <Divider />
        <List>{getAuthorizedNavigation()}</List>
        <Divider />
      </Drawer>
      <main className={classes.content}>
        <div className={classes.toolbar} />
        <Typography
          style={{
            fontWeight: "bold",
            color: "var(--secondary)",
            fontFamily: "DaggerSquare",
            marginBottom: "1rem",
          }}
          variant="h5"
        >
          {`${currentLocation.pathname
            .replace("/", "")
            .replace("-", " ")
            .split("/")[0]
            .toUpperCase()}`}
        </Typography>
        <Switch>{getAuthorizedContent()}</Switch>
      </main>
    </div>
  );
}
export default withRouter(Panel);
