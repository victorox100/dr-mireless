import React, { ChangeEvent } from "react";
import UserForm from "../../Components/Form/User";
import Add from "../../Components/ActionModals/Add";
import {
  Button,
  ButtonGroup,
  Avatar,
  FormControl,
  InputLabel,
  Select,
  Grid,
  Tooltip,
} from "@material-ui/core";
import Table from "../../Components/DataVisual/Table";
import EditIcon from "@material-ui/icons/Edit";
import SuperAdmin from "../../Classes/SuperAdmin";
import ICommonData from "../../Classes/Interfaces/ICommonData";
import ITerapist from "../../Classes/Interfaces/ITerapist";
import ILocation from "../../Classes/Interfaces/ILocation";
import UserContext from "../../Context/User";
import withAlert from "../../Components/HOC/withAlert";
import { AlertMessage } from "../../Components/ActionModals/Toast";
import Pagination from "../../Components/Buttons/Pagination";
import Terapist from "../../Classes/Terapist";
import moment from "moment";
interface State extends ICommonData, Omit<ITerapist, "birthday"> {
  open: Boolean;
  edit: Boolean;
  users: Array<ICommonData>;
  password_confirm: String;
  password: String;
  type: 2 | 4 | 5;
  pagination: any;
  locations: ILocation[];
  selectedLocation: Number;
  birthday: Date;
}
class RegisterUsers extends React.Component<
  {
    showAlert: (alert: AlertMessage) => void;
    showError: Function;
  },
  State
> {
  static contextType = UserContext;
  context!: React.ContextType<typeof UserContext>;
  state = {
    id: 0,
    first_name: "",
    last_name: "",
    phone: "",
    email: "",
    gender: 1,
    birthday: moment().toDate(),
    specialty: "",
    professional_license: "",
    specialty_license: "",
    password: "",
    password_confirm: "",
    open: false,
    users: [],
    edit: false,
    type: 2 as 2 | 4 | 5,
    page: 1,
    pagination: { first: 0, last: 0, next: 0, prev: 0 },
    locations: [],
    selectedLocation: 0,
  };
  async componentDidMount() {
    this.getUsersPaginated(1);
    const admin: SuperAdmin = this.context.user as SuperAdmin;
    const response: any = await admin.getLocations(1, true);
    if (response) {
      this.setState({
        locations: response.data,
        selectedLocation: response.data[0].id,
      });
    } else {
      this.props.showError();
    }
  }

  getUsersPaginated = async (page: Number) => {
    const admin: SuperAdmin = this.context.user as SuperAdmin;
    const response: any = await admin.getUsers(this.state.type, page);
    if (response) {
      this.setState({ users: response.data, pagination: response.pagination });
    }
  };
  addUser = async () => {
    const admin: SuperAdmin = this.context.user as SuperAdmin;
    let user: (ITerapist | ICommonData) & {
      password: String;
      password_confirm: String;
    };
    switch (this.state.type) {
      case 4:
        user = {
          id: 0,
          first_name: this.state.first_name,
          last_name: this.state.last_name,
          phone: this.state.phone,
          email: this.state.email,
          gender: this.state.gender,
          birthday: moment(this.state.birthday).format("YYYY-MM-DD"),
          specialty: this.state.specialty,
          professional_license: this.state.professional_license,
          specialty_license: this.state.specialty_license,
          password: this.state.password,
          password_confirm: this.state.password_confirm,
          location_id: this.state.selectedLocation,
        } as ITerapist & { password_confirm: String; password: String };
        break;
      default:
        user = {
          id: 0,
          first_name: this.state.first_name,
          last_name: this.state.last_name,
          phone: this.state.phone,
          email: this.state.email,
          password: this.state.password,
          password_confirm: this.state.password_confirm,
          location_id: this.state.selectedLocation,
        } as ICommonData & { password_confirm: String; password: String };
    }
    const newUser = await admin.registerUser(
      this.state.type,
      user,
      this.state.selectedLocation
    );
    if (newUser) {
      const { users } = this.state as any;
      users.push(newUser);
      this.setState({ users: users, open: false }, () =>
        this.props.showAlert({
          message: "Se ha agregado el usuario",
          severity: "success",
        })
      );
    } else {
      this.props.showError();
    }
  };

  updateUser = async () => {
    const admin: SuperAdmin = this.context.user as SuperAdmin;
    let user: ITerapist | ICommonData;
    switch (this.state.type) {
      case 4:
        user = {
          id: this.state.id,
          first_name: this.state.first_name,
          last_name: this.state.last_name,
          phone: this.state.phone,
          email: this.state.email,
          gender: this.state.gender,
          birthday: moment(this.state.birthday).format("YYYY-MM-DD"),
          specialty: this.state.specialty,
          professional_license: this.state.professional_license,
          specialty_license: this.state.specialty_license,
          password: this.state.password,
          password_confirm: this.state.password_confirm,
        } as ITerapist;
        break;
      default:
        user = {
          id: this.state.id,
          first_name: this.state.first_name,
          last_name: this.state.last_name,
          phone: this.state.phone,
          email: this.state.email,
          password: this.state.password,
          password_confirm: this.state.password_confirm,
        } as ICommonData;
    }
    const updatedUser = await admin.updateUser(
      user,
      this.state.selectedLocation
    );
    if (updatedUser) {
      const { users } = this.state as any;
      const updated = users.map((currentUser: ITerapist) => {
        if (currentUser.id === updatedUser.id) {
          return updatedUser;
        } else {
          return currentUser;
        }
      });
      this.setState(
        { password: "", password_confirm: "", users: updated, open: false },
        () =>
          this.props.showAlert({
            message: "Se ha actualizado el usuario",
            severity: "success",
          })
      );
    } else {
      this.props.showError();
    }
  };
  handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    const key = event.currentTarget.name;
    if (Object.keys(this.state).includes(key)) {
      if (key === "gender") {
        this.setState({ gender: parseInt(event.currentTarget.value) });
      } else {
        this.setState(({ [key]: event.currentTarget.value } as unknown) as Pick<
          State,
          keyof State
        >);
      }
    }
  };
  handleBirthdayChange = (date: any) => {
    this.setState({ birthday: date });
  };
  render() {
    return (
      <>
        <div>
          <Grid container justify="space-around">
            <Grid item xs={6}>
              <Add
                handleModal={() => {
                  this.setState({
                    open: !this.state.open,
                    edit: false,
                    id: 0,
                    first_name: "",
                    last_name: "",
                    phone: "",
                    email: "",
                    gender: 0,
                    birthday: moment().toDate(),
                    specialty: "",
                    professional_license: "",
                    specialty_license: "",
                    password: "",
                    password_confirm: "",
                  });
                }}
                open={this.state.open}
              >
                <>
                  <UserForm
                    handleBirthdayChange={this.handleBirthdayChange}
                    handleChange={this.handleChange}
                    handleLocationChange={(
                      event: React.ChangeEvent<{
                        name?: string;
                        value: unknown;
                      }>
                    ) =>
                      this.setState({
                        selectedLocation: parseInt(
                          event.target.value as string
                        ),
                      })
                    }
                    {...this.state}
                    location={this.state.selectedLocation}
                  />
                  <ButtonGroup
                    style={{
                      marginTop: "2rem",
                      width: "100%",
                      display: "flex",
                      justifyContent: "center",
                      position: "absolute",
                      bottom: 10,
                    }}
                    size="large"
                    color="default"
                  >
                    <Button
                      onClick={this.state.edit ? this.updateUser : this.addUser}
                      disabled={
                        (!this.state.edit && this.state.password.length < 8) ||
                        (!this.state.edit &&
                          this.state.password !== this.state.password_confirm)
                      }
                    >
                      {this.state.edit ? "Actualizar" : "Agregar"}
                    </Button>
                    <Button
                      onClick={() =>
                        this.setState({
                          open: !this.state.open,
                          edit: false,
                          id: 0,
                          first_name: "",
                          last_name: "",
                          phone: "",
                          email: "",
                          gender: 0,
                          birthday: new Date(),
                          specialty: "",
                          professional_license: "",
                          specialty_license: "",
                          password: "",
                          password_confirm: "",
                        })
                      }
                    >
                      Cancelar
                    </Button>
                  </ButtonGroup>
                </>
              </Add>
            </Grid>

            <Grid item xs={6}>
              <FormControl style={{ width: "50%" }}>
                <InputLabel htmlFor="type">Tipo de usuario</InputLabel>
                <Select
                  native
                  value={this.state.type}
                  onChange={(
                    event: React.ChangeEvent<{ name?: string; value: unknown }>
                  ) =>
                    this.setState(
                      {
                        type: parseInt(event.target.value as string) as
                          | 2
                          | 4
                          | 5,
                      },
                      () => this.getUsersPaginated(1)
                    )
                  }
                  inputProps={{
                    name: "type",
                    id: "type",
                    style: { width: "100%" },
                  }}
                >
                  <option
                    style={{ fontWeight: "bold", backgroundColor: "white" }}
                    value={2}
                  >
                    Staff
                  </option>
                  <option
                    style={{ fontWeight: "bold", backgroundColor: "white" }}
                    value={4}
                  >
                    Terapeuta
                  </option>
                  <option
                    style={{ fontWeight: "bold", backgroundColor: "white" }}
                    value={5}
                  >
                    Secretaria/Asistente
                  </option>
                </Select>
              </FormControl>
            </Grid>
          </Grid>
          <Table
            model={"ID"}
            attributes={
              this.state.type === 4
                ? [
                    this.state.type === 4
                      ? "Terapeuta"
                      : this.state.type === 2
                      ? "Staff"
                      : "Secretaria/Asistente",
                    "Teléfono",
                    "Correo electrónico",
                    "Sexo",
                    "Fecha de Nacimiento",
                    "Especialidad",
                    "Cédula Profesional",
                    "Cédula de Especialidad",
                    "Acciones",
                  ]
                : [
                    this.state.type === 2 ? "Staff" : "Secretaria/Asistente",
                    "Correo electrónico",
                    "Teléfono",
                    "Acciones",
                  ]
            }
            data={this.state.users.map((user: any): {
              name: String;
              values: any;
            } => ({
              name: user.id,
              values:
                this.state.type === 4
                  ? [
                      `${user.first_name} ${user.last_name}`,
                      user.phone,
                      user.email,
                      user.gender === 2 ? "Mujer" : "Hombre",
                      user.birthday
                        ? user.birthday.split("-").reverse().join("/")
                        : user.birthday,
                      user.specialty,
                      user.professional_license,
                      user.specialty_license,
                      <Grid container direction="row" justify="flex-start">
                        <Avatar
                          style={{
                            cursor: "pointer",
                            backgroundColor: "var(--secondary)",
                          }}
                          onClick={async () => {
                            const worker = new Terapist(user);
                            await worker.setLocation();
                            this.setState({
                              ...(worker as any),
                              selectedLocation: worker.location_id as Number,
                              birthday: new Date(worker.birthday as string),
                              open: true,
                              edit: true,
                            });
                          }}
                        >
                          <EditIcon />
                        </Avatar>
                      </Grid>,
                    ]
                  : [
                      `${user.first_name} ${user.last_name}`,
                      user.email,
                      user.phone,
                      <Grid container justify="flex-start" direction="row">
                        <Tooltip title="Editar">
                          <Avatar
                            style={{
                              cursor: "pointer",
                              backgroundColor: "var(--secondary)",
                            }}
                            onClick={async () => {
                              const worker = new Terapist(user);
                              await worker.setLocation();
                              this.setState({
                                ...(worker as any),
                                selectedLocation: worker.location_id as Number,
                                birthday: new Date(worker.birthday as string),
                                open: true,
                                edit: true,
                              });
                            }}
                          >
                            <EditIcon />
                          </Avatar>
                        </Tooltip>
                      </Grid>,
                    ],
            }))}
          />
        </div>
        <Grid container justify="center">
          <Grid container justify="space-between" item sm={4} xs={8}>
            <Pagination
              pagination={this.state.pagination}
              getData={this.getUsersPaginated}
            />
          </Grid>
        </Grid>
      </>
    );
  }
}
export default withAlert(RegisterUsers);
