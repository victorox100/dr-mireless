import React from "react";
import Card from "../Draggable/Card";
import update from "immutability-helper";

export interface Item {
  id: number;
  text: string;
}
export interface ContainerState {
  cards: Item[];
}
interface Props {
  data: any;
  setCards: Function;
  deleteAudio: Function;
}
export const Container = (props: Props) => {
  const moveCard = (dragIndex: number, hoverIndex: number) => {
    const dragCard = props.data[dragIndex];
    props.setCards(
      update(props.data, {
        $splice: [
          [dragIndex, 1],
          [hoverIndex, 0, dragCard],
        ],
      })
    );
  };

  return (
    <div>
      {props.data.map((audio: any, i: number) => (
        <Card
          key={audio.id}
          index={i}
          id={audio.id}
          text={audio.name}
          moveCard={moveCard}
          remove={props.deleteAudio}
        />
      ))}
    </div>
  );
};
