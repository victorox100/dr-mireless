import React from "react";
import {
  Grid,
  TextField,
  Typography,
  Button,
  InputAdornment,
  Avatar,
  Tooltip,
} from "@material-ui/core";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import UserContext from "../../Context/User";
import SuperAdmin from "../../Classes/SuperAdmin";
import Secretary from "../../Classes/Secretary";
import withAlert, { withAlertProps } from "../HOC/withAlert";
import IClinicalHistory from "../../Classes/Interfaces/IClinicalHistory";
import Terapist from "../../Classes/Terapist";
import EditIcon from "@material-ui/icons/Edit";
import SaveIcon from "@material-ui/icons/Save";

interface ClinicalHistoryProps extends withAlertProps {
  customer_id: Number;
}

const styles: any = {
  textField: {
    width: "100%",
  },
  bold: {
    fontWeight: "bold",
    marginTop: "0.5rem",
    marginBottom: "0.5rem",
  },
};
const ClinicalHistory: React.FC<ClinicalHistoryProps> = (
  props: ClinicalHistoryProps
) => {
  const [history_id, setID] = React.useState<Number>(0);
  const [medicines, setMedicines] = React.useState<String>("No");
  const [surgery, setSurgery] = React.useState<String>("No");
  const [smoking, setSmoking] = React.useState<String>("No");
  const [alcoholic, setAlcoholic] = React.useState<String>("No");
  const [drugs, setDrugs] = React.useState<String>("No");
  const [exercise, setExercise] = React.useState<String>("No");
  const [diabetis, setDiabetis] = React.useState<String>("No"); // type 1
  const [hyper, setHyper] = React.useState<String>("No"); //type 1
  const [other1, setOther1] = React.useState<String>(""); //type 1
  const [other2, setOther2] = React.useState<String>(""); // type 2
  const [diabetisFam, setDiabetisFam] = React.useState<String>("No"); // type 3
  const [hyperFam, setHyperFam] = React.useState<String>("No"); //type 3
  const [cancerFam, setCancerFam] = React.useState<String>("No");
  const [otherFam, setOtherFam] = React.useState<String>("");
  const [height, setHeight] = React.useState<Number>(0);
  const [weight, setWeight] = React.useState<Number>(0);
  const [blood_pressure, setBloodPressure] = React.useState<Number>(0);
  const [body_temperature, setTemperature] = React.useState<Number>(0);
  const [heart_rate, setHeartRate] = React.useState<Number>(0);
  const [breathing_frequency, setBreathFreq] = React.useState<Number>(0);
  const [isEditing, setEditing] = React.useState<Boolean>(false);
  const userContext = React.useContext<UserContext>(UserContext);

  React.useEffect(() => {
    getClinicalHistory();
  }, [props.customer_id]);

  const editClinicalHistory = async () => {
    const clinicalHistory: IClinicalHistory = {
      id: history_id,
      other_fields: [
        {
          type: 1,
          diabetes: diabetis,
          hypertension: hyper,
          other: other1,
        },
        {
          type: 2,
          other: other2,
        },
        {
          type: 3,
          diabetes: diabetisFam,
          cancer: cancerFam,
          hypertension: hyperFam,
          other: otherFam,
        },
      ],
      date: new Date().toISOString(),
      medicines: medicines,
      surgeries: surgery,
      smoking: smoking,
      alcoholic: alcoholic,
      drugs: drugs,
      exercise: exercise,
      user: props.customer_id,
      height: height,
      weight: weight,
      blood_pressure: blood_pressure,
      body_temperature: body_temperature,
      heart_rate: heart_rate,
      breathing_frequency: breathing_frequency,
    };
    const worker = userContext.user as SuperAdmin | Secretary | Terapist;
    const response =
      clinicalHistory.id === 0
        ? await worker.createClinicalHistory(clinicalHistory)
        : await worker.updateClinicalHistory(clinicalHistory);
    if (response) {
      props.showAlert({
        message:
          clinicalHistory.id === 0
            ? "Se ha creado el historial"
            : "Se ha actualizado el historial",
        severity: "success",
      });
    } else {
      props.showError();
    }
    setEditing(false);
  };

  const getClinicalHistory = async () => {
    props.showAlert({
      message: "Se está cargando la información del paciente",
      severity: "info",
    });
    const worker = userContext.user as SuperAdmin | Secretary | Terapist;
    const clinicalHistory = await worker.getClinicalHistory(props.customer_id);
    if (clinicalHistory) {
      if ((clinicalHistory as IClinicalHistory[]).length > 0) {
        const record: IClinicalHistory = (clinicalHistory as IClinicalHistory[])[0];
        setID(record.id as Number);
        setMedicines(record.medicines);
        setSurgery(record.surgeries);
        setSmoking(record.smoking);
        setAlcoholic(record.alcoholic);
        setDrugs(record.drugs);
        setExercise(record.exercise);
        setDiabetis(
          record.other_fields.find((fields) => fields.type === 1)?.diabetes ||
            ""
        );
        setHyper(
          record.other_fields.find((fields) => fields.type === 1)
            ?.hypertension || ""
        );
        setOther1(
          record.other_fields.find((fields) => fields.type === 1)?.other || ""
        );
        setOther2(
          record.other_fields.find((fields) => fields.type === 2)?.other || ""
        );
        setDiabetisFam(
          record.other_fields.find((fields) => fields.type === 3)?.diabetes ||
            ""
        );
        setHyperFam(
          record.other_fields.find((fields) => fields.type === 3)
            ?.hypertension || ""
        );
        setCancerFam(
          record.other_fields.find((fields) => fields.type === 3)?.cancer || ""
        );
        setOtherFam(
          record.other_fields.find((fields) => fields.type === 3)?.other || ""
        );
        setHeight(record.height);
        setWeight(record.weight);
        setBloodPressure(record.blood_pressure);
        setTemperature(record.body_temperature);
        setHeartRate(record.heart_rate);
        setBreathFreq(record.breathing_frequency);
        props.showAlert({
          message: "¡Listo!",
          severity: "success",
        });
      } else {
        props.showAlert({
          message: "Aún no se crea un historial clínico",
          severity: "info",
        });
      }
    } else {
      props.showError();
    }
  };

  return (
    <Grid
      container
      style={{ overflowY: "auto", maxHeight: "60vh", position: "relative" }}
      spacing={2}
    >
      <Grid xs={12}>
        <Typography style={styles.bold} variant="h6">
          Información general
        </Typography>
      </Grid>
      <Grid item sm={3} xs={6}>
        <TextField
          disabled={!isEditing}
          label="Altura"
          type="number"
          InputProps={{
            endAdornment: <InputAdornment position="end">cm</InputAdornment>,
          }}
          value={height}
          onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
            setHeight(parseInt((event.target as HTMLInputElement).value));
          }}
        />
      </Grid>
      <Grid item sm={3} xs={6}>
        <TextField
          disabled={!isEditing}
          label="Peso"
          type="number"
          InputProps={{
            endAdornment: <InputAdornment position="end">Kg</InputAdornment>,
          }}
          value={weight}
          onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
            setWeight(parseInt((event.target as HTMLInputElement).value));
          }}
        />
      </Grid>
      <Grid item sm={3} xs={6}>
        <TextField
          disabled={!isEditing}
          label="Tensión arterial"
          InputProps={{
            endAdornment: <InputAdornment position="end">mmHg</InputAdornment>,
          }}
          value={blood_pressure}
          onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
            setBloodPressure(
              parseInt((event.target as HTMLInputElement).value)
            );
          }}
        />
      </Grid>
      <Grid item sm={3} xs={6}>
        <TextField
          disabled={!isEditing}
          label="Temperatura corporal"
          type="number"
          InputProps={{
            endAdornment: <InputAdornment position="end">ºC</InputAdornment>,
          }}
          value={body_temperature}
          onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
            setTemperature(parseInt((event.target as HTMLInputElement).value));
          }}
        />
      </Grid>
      <Grid item sm={4} xs={6}>
        <TextField
          disabled={!isEditing}
          label="Frecuencia cardiaca"
          type="number"
          InputProps={{
            endAdornment: (
              <InputAdornment position="end">ciclos/minuto</InputAdornment>
            ),
          }}
          value={heart_rate}
          onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
            setHeartRate(parseInt((event.target as HTMLInputElement).value));
          }}
        />
      </Grid>
      <Grid item sm={4} xs={6}>
        <TextField
          disabled={!isEditing}
          label="Frecuencia respiratoria"
          type="number"
          InputProps={{
            endAdornment: (
              <InputAdornment position="end">ciclos/minuto</InputAdornment>
            ),
          }}
          value={breathing_frequency}
          onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
            setBreathFreq(parseInt((event.target as HTMLInputElement).value));
          }}
        />
      </Grid>
      <Grid xs={12}>
        <Typography style={styles.bold} variant="h6">
          Antecedentes Personales
        </Typography>
      </Grid>
      <Grid sm={3} xs={6}>
        <FormControl component="fieldset">
          <FormLabel component="legend">Medicamentos</FormLabel>
          <RadioGroup
            aria-label="medicinas"
            name="medicinas"
            value={medicines}
            onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
              setMedicines((event.target as HTMLInputElement).value);
            }}
          >
            <FormControlLabel
              disabled={!isEditing}
              value="Si"
              control={<Radio />}
              label="Sí"
            />
            <FormControlLabel
              disabled={!isEditing}
              value="No"
              control={<Radio />}
              label="No"
            />
          </RadioGroup>
        </FormControl>
      </Grid>
      <Grid sm={3} xs={6}>
        <FormControl component="fieldset">
          <FormLabel component="legend">Cirugías</FormLabel>
          <RadioGroup
            aria-label="cirugias"
            name="cirugias"
            value={surgery}
            onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
              setSurgery((event.target as HTMLInputElement).value);
            }}
          >
            <FormControlLabel
              disabled={!isEditing}
              value="Si"
              control={<Radio />}
              label="Sí"
            />
            <FormControlLabel
              disabled={!isEditing}
              value="No"
              control={<Radio />}
              label="No"
            />
          </RadioGroup>
        </FormControl>
      </Grid>
      <Grid sm={3} xs={6}>
        <FormControl component="fieldset">
          <FormLabel component="legend">Diabetis mellitus</FormLabel>
          <RadioGroup
            aria-label="diabetis"
            name="diabetis"
            value={diabetis}
            onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
              setDiabetis((event.target as HTMLInputElement).value);
            }}
          >
            <FormControlLabel
              disabled={!isEditing}
              value="Si"
              control={<Radio />}
              label="Sí"
            />
            <FormControlLabel
              disabled={!isEditing}
              value="No"
              control={<Radio />}
              label="No"
            />
          </RadioGroup>
        </FormControl>
      </Grid>
      <Grid sm={3} xs={6}>
        <FormControl component="fieldset">
          <FormLabel component="legend">Hipertensión arterial</FormLabel>
          <RadioGroup
            aria-label="hipertension"
            name="hipertension"
            value={hyper}
            onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
              setHyper((event.target as HTMLInputElement).value);
            }}
          >
            <FormControlLabel
              disabled={!isEditing}
              value="Si"
              control={<Radio />}
              label="Sí"
            />
            <FormControlLabel
              disabled={!isEditing}
              value="No"
              control={<Radio />}
              label="No"
            />
          </RadioGroup>
        </FormControl>
      </Grid>
      <Grid sm={6} xs={12}>
        <TextField
          disabled={!isEditing}
          style={styles.textField}
          label="Otro"
          value={other1}
          multiline
          rows={3}
          onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
            setOther1((event.target as HTMLInputElement).value);
          }}
        />
      </Grid>
      <Grid xs={12}>
        <Typography style={styles.bold} variant="h6">
          Antecedentes Personales No Patológicos
        </Typography>
      </Grid>
      <Grid sm={3} xs={6}>
        <FormControl component="fieldset">
          <FormLabel component="legend">Tabaquismo</FormLabel>
          <RadioGroup
            aria-label="fuma"
            name="fuma"
            value={smoking}
            onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
              setSmoking((event.target as HTMLInputElement).value);
            }}
          >
            <FormControlLabel
              disabled={!isEditing}
              value="Si"
              control={<Radio />}
              label="Sí"
            />
            <FormControlLabel
              disabled={!isEditing}
              value="No"
              control={<Radio />}
              label="No"
            />
          </RadioGroup>
        </FormControl>
      </Grid>
      <Grid sm={3} xs={6}>
        <FormControl component="fieldset">
          <FormLabel component="legend">Toma bebidas alcohólicas</FormLabel>
          <RadioGroup
            aria-label="alcohol"
            name="alcohol"
            value={alcoholic}
            onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
              setAlcoholic((event.target as HTMLInputElement).value);
            }}
          >
            <FormControlLabel
              disabled={!isEditing}
              value="Si"
              control={<Radio />}
              label="Sí"
            />
            <FormControlLabel
              disabled={!isEditing}
              value="No"
              control={<Radio />}
              label="No"
            />
          </RadioGroup>
        </FormControl>
      </Grid>
      <Grid sm={3} xs={6}>
        <FormControl component="fieldset">
          <FormLabel component="legend">Consume drogas</FormLabel>
          <RadioGroup
            aria-label="drogadicto"
            name="drogadicto"
            value={drugs}
            onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
              setDrugs((event.target as HTMLInputElement).value);
            }}
          >
            <FormControlLabel
              disabled={!isEditing}
              value="Si"
              control={<Radio />}
              label="Sí"
            />
            <FormControlLabel
              disabled={!isEditing}
              value="No"
              control={<Radio />}
              label="No"
            />
          </RadioGroup>
        </FormControl>
      </Grid>
      <Grid sm={3} xs={6}>
        <FormControl component="fieldset">
          <FormLabel component="legend">Realiza ejercicio</FormLabel>
          <RadioGroup
            aria-label="ejercicio"
            name="ejercicio"
            value={exercise}
            onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
              setExercise((event.target as HTMLInputElement).value);
            }}
          >
            <FormControlLabel
              disabled={!isEditing}
              value="Si"
              control={<Radio />}
              label="Sí"
            />
            <FormControlLabel
              disabled={!isEditing}
              value="No"
              control={<Radio />}
              label="No"
            />
          </RadioGroup>
        </FormControl>
      </Grid>
      <Grid sm={6} xs={12}>
        <TextField
          disabled={!isEditing}
          style={styles.textField}
          label="Otros"
          value={other2}
          multiline
          rows={3}
          onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
            setOther2((event.target as HTMLInputElement).value);
          }}
        />
      </Grid>
      <Grid xs={12}>
        <Typography style={styles.bold} variant="h6">
          Antecedentes Familiares
        </Typography>
      </Grid>
      <Grid sm={3} xs={6}>
        <FormControl component="fieldset">
          <FormLabel component="legend">Diabetis mellitus</FormLabel>
          <RadioGroup
            aria-label="diabetis"
            name="diabetis"
            value={diabetisFam}
            onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
              setDiabetisFam((event.target as HTMLInputElement).value);
            }}
          >
            <FormControlLabel
              disabled={!isEditing}
              value="Si"
              control={<Radio />}
              label="Sí"
            />
            <FormControlLabel
              disabled={!isEditing}
              value="No"
              control={<Radio />}
              label="No"
            />
          </RadioGroup>
        </FormControl>
      </Grid>
      <Grid sm={3} xs={6}>
        <FormControl component="fieldset">
          <FormLabel component="legend">Cáncer</FormLabel>
          <RadioGroup
            aria-label="hipertension"
            name="hipertension"
            value={cancerFam}
            onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
              setCancerFam((event.target as HTMLInputElement).value);
            }}
          >
            <FormControlLabel
              disabled={!isEditing}
              value="Si"
              control={<Radio />}
              label="Sí"
            />
            <FormControlLabel
              disabled={!isEditing}
              value="No"
              control={<Radio />}
              label="No"
            />
          </RadioGroup>
        </FormControl>
      </Grid>
      <Grid sm={3} xs={6}>
        <FormControl component="fieldset">
          <FormLabel component="legend">Hipertensión arterial</FormLabel>
          <RadioGroup
            aria-label="hipertension"
            name="hipertension"
            value={hyperFam}
            onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
              setHyperFam((event.target as HTMLInputElement).value);
            }}
          >
            <FormControlLabel
              disabled={!isEditing}
              value="Si"
              control={<Radio />}
              label="Sí"
            />
            <FormControlLabel
              disabled={!isEditing}
              value="No"
              control={<Radio />}
              label="No"
            />
          </RadioGroup>
        </FormControl>
      </Grid>
      <Grid sm={6} xs={12}>
        <TextField
          disabled={!isEditing}
          style={styles.textField}
          label="Otro"
          value={otherFam}
          multiline
          rows={3}
          onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
            setOtherFam((event.target as HTMLInputElement).value);
          }}
        />
      </Grid>
      {userContext.user instanceof Terapist && (
        <Grid item xs={12}>
          <Button
            variant="contained"
            color="primary"
            onClick={isEditing ? editClinicalHistory : () => setEditing(true)}
          >
            {isEditing ? "Actualizar" : "Editar"}
          </Button>
        </Grid>
      )}
      {userContext.user instanceof Secretary && (
        <div style={{ position: "absolute", top: 0, right: "2.5rem" }}>
          <Tooltip title={isEditing ? "Guardar" : "Editar"}>
            <Avatar
              style={{
                cursor: "pointer",
                position: "fixed",
                backgroundColor: isEditing
                  ? "var(--principal)"
                  : "var(--secondary)",
              }}
            >
              {!isEditing && <EditIcon onClick={() => setEditing(true)} />}
              {isEditing && <SaveIcon onClick={editClinicalHistory} />}
            </Avatar>
          </Tooltip>
        </div>
      )}
    </Grid>
  );
};
export default withAlert(ClinicalHistory);
