import React, { useEffect } from "react";
import { withRouter, RouteComponentProps } from "react-router-dom";
import { DateTimePicker } from "@material-ui/pickers";
import {
  Typography,
  Grid,
  Chip,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  Button,
  Avatar,
} from "@material-ui/core";
import DateFnsUtils from "@date-io/date-fns";
import { es } from "date-fns/locale";
import { MuiPickersUtilsProvider } from "@material-ui/pickers";
import ITerapist from "../../Classes/Interfaces/ITerapist";
import ICustomer from "../../Classes/Interfaces/ICustomer";
import { createMuiTheme } from "@material-ui/core";
import { ThemeProvider } from "@material-ui/styles";
import { MuiPickersOverrides } from "@material-ui/pickers/typings/overrides";
import { Edit, AddAlert } from "@material-ui/icons";
import { MaterialUiPickersDate } from "@material-ui/pickers/typings/date";
import UserContext from "../../Context/User";
import Secretary from "../../Classes/Secretary";
import SuperAdmin from "../../Classes/SuperAdmin";
import Pagination from "../Buttons/Pagination";

const materialTheme = createMuiTheme({
  overrides: {
    MuiPickersToolbar: {
      toolbar: {
        backgroundColor: "var(--principal)",
      },
    },
    MuiPickersDay: {
      day: {
        color: "var(--secondary)",
      },
      daySelected: {
        backgroundColor: "var(--principal)",
      },
      current: {
        color: "var(--principal)",
      },
    },
  },
});

interface Props extends RouteComponentProps {
  id?: Number;
  patient: Number;
  edit: Boolean;
  customer: String;
  getPage: (page: Number) => void;
  pagination: any;
  terapists: ITerapist[];
  date: Date;
  changeDate: (date: MaterialUiPickersDate) => void;
  selected: Number;
  select: (id: Number) => void;
  status: Number;
  changeStatus: (e: React.ChangeEvent<{ value: unknown }>) => void;
}

export default withRouter(function Appointment(props: Props) {
  const [isEditing, setEditing] = React.useState(false);
  const [terapist, setTerapist] = React.useState("");
  const context = React.useContext<UserContext>(UserContext);
  useEffect(() => {
    getTerapist();
  }, [props.selected]);

  const getTerapist = async () => {
    const worker = context.user as Secretary | SuperAdmin;
    const response = await worker.getCustomer(props.selected);
    return response
      ? setTerapist(
          `${(response as ICustomer).first_name} ${
            (response as ICustomer).last_name
          }`
        )
      : setTerapist("");
  };
  return (
    <div
      style={
        {
          backgroundColor: "white",
          height: "60vh",
        } as React.CSSProperties
      }
    >
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <Typography variant="h6">
            {props.edit ? "Actualizar cita" : "Nueva cita"}
          </Typography>
        </Grid>
        <Grid item xs={12} style={{ position: "relative" }}>
          <Typography
            style={{
              width: "100%",
              marginBottom: "0.1rem",
              fontWeight: "bold",
            }}
            variant="body1"
          >
            Nombre del paciente : {props.customer}
          </Typography>
          {props.id ? (
            <Avatar
              onClick={() => props.history.push(`notificaciones/1/${props.id}`)}
              style={{ cursor: "pointer", backgroundColor: "var(--secondary)" }}
            >
              <AddAlert />
            </Avatar>
          ) : null}
        </Grid>
        <Grid item sm={6} xs={12}>
          {props.edit && !isEditing ? (
            <Typography variant="body1">
              Terapeuta:
              <Chip
                onDelete={() => {
                  setEditing(true);
                }}
                deleteIcon={<Edit />}
                label={terapist}
              />
            </Typography>
          ) : (
            <>
              <Typography
                style={{ width: "100%", textAlign: "center" }}
                variant="body1"
              >
                Seleccionar un terapeuta
              </Typography>
              <div style={{ maxHeight: "70%", overflowY: "auto" }}>
                {props.terapists.map((terapist: ITerapist) => (
                  <Chip
                    style={{
                      width: "100%",
                      paddingRight: "1rem",
                      paddingLeft: "1rem",
                      marginBottom: "0.1rem",
                      textAlign: "center",
                      backgroundColor:
                        props.selected === terapist.id
                          ? "var(--principal)"
                          : "#dfdfdf",
                      color: props.selected === terapist.id ? "white" : "black",
                    }}
                    label={`${terapist.first_name} ${terapist.last_name}`}
                    onClick={() => props.select(terapist.id)}
                  />
                ))}
              </div>
              <Grid container justify="center">
                <Pagination
                  getData={props.getPage}
                  pagination={props.pagination}
                />
              </Grid>
            </>
          )}
        </Grid>
        <Grid item container direction="column" justify="center" sm={6} xs={12}>
          <MuiPickersUtilsProvider utils={DateFnsUtils} locale={es}>
            <ThemeProvider theme={materialTheme}>
              <DateTimePicker
                format="dd-MM-yyyy HH:mm"
                variant="inline"
                openTo="hours"
                value={props.date}
                minutesStep={15}
                onChange={props.changeDate}
                label="Fecha y hora"
                disablePast
                shouldDisableDate={(date: MaterialUiPickersDate) =>
                  date!.getDay() === 0
                }
              />
            </ThemeProvider>
          </MuiPickersUtilsProvider>
          {props.edit && (
            <FormControl style={{ marginTop: "1.5rem" }}>
              <InputLabel id="demo-simple-select-helper-label">
                Estado de la cita:
              </InputLabel>
              <Select
                labelId="demo-simple-select-helper-label"
                id="demo-simple-select-helper"
                value={props.status}
                onChange={props.changeStatus}
              >
                <MenuItem value={1}>Cancelado</MenuItem>
                <MenuItem value={2}>Pendiente</MenuItem>
                <MenuItem value={3}>Confirmado</MenuItem>

                <MenuItem value={4}>Atendido</MenuItem>
                <MenuItem value={5}>No atendido</MenuItem>
              </Select>
            </FormControl>
          )}
          {props.status === 4 && props.patient && (
            <Button
              style={{
                marginTop: "0.5rem",
                backgroundColor: "var(--secondary)",
                color: "white",
              }}
              onClick={() =>
                props.history.push(`registrar-pagos/${props.patient}`)
              }
            >
              Crear registro de pago
            </Button>
          )}
        </Grid>
      </Grid>
    </div>
  );
});
type overridesNameToClassKey = {
  [P in keyof MuiPickersOverrides]: keyof MuiPickersOverrides[P];
};

declare module "@material-ui/core/styles/overrides" {
  export interface ComponentNameToClassKey extends overridesNameToClassKey {}
}
