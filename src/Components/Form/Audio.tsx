import React from "react";
import { Container, Grid, TextField, Typography } from "@material-ui/core";
import IAudio from "../../Classes/Interfaces/IAudio";
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";
import Upload from "../Buttons/Upload";
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    input: {
      width: "100%",
    },
  })
);
interface Props extends IAudio {
  handleChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
}
const AddCustomer = (props: Props) => {
  const classes = useStyles();
  return (
    <form style={{ paddingTop: "3rem" }}>
      <Container maxWidth="md">
        <Grid container alignItems="center" spacing={2}>
          <Grid item sm={6} xs={12}>
            <TextField
              className={classes.input}
              label="Nombre de la biofrecuencia"
              value={props.name}
              onChange={props.handleChange}
              name="name"
            />
          </Grid>
          <Grid item md={6} xs={12}>
            <Upload
              style={{ width: "100%", backgroundColor: "var(--secondary)" }}
              text={
                typeof props.audio === "string"
                  ? "Reemplazar biofrecuencia"
                  : "Subir biofrecuencia"
              }
              multiple={false}
              onChange={props.handleChange}
              name="audio"
              accept="audio/mp3"
            />
          </Grid>
          <Grid item md={6} xs={12}>
            {props.audio instanceof File ? (
              <Typography variant="body1">
                Archivo seleccionado: {props.audio.name}
              </Typography>
            ) : (
              <Typography variant="body1">
                No se ha seleccionado ningún archivo
              </Typography>
            )}
          </Grid>
          <Grid item container alignItems="center" sm={6} xs={12}>
            <Typography variant="body1">
              Duración: {props.duration.toFixed(2)} segundos
            </Typography>
          </Grid>
          {typeof props.audio === "string" ? (
            <Grid
              item
              container
              direction="column"
              alignItems="flex-start"
              md={6}
              xs={12}
            >
              <Typography style={{ marginBottom: "1rem" }} variant="body1">
                Archivo actual
              </Typography>
              <audio
                style={{
                  backgroundColor: "var(--secondary)",
                  borderRadius: 20,
                  boxShadow: "#00000040 0px 0px 4px 5px",
                }}
                controls
                src={props.audio}
              />
            </Grid>
          ) : null}
        </Grid>
      </Container>
    </form>
  );
};
export default AddCustomer;
