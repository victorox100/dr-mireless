import React from "react";
import Add from "../ActionModals/Add";
import { Container, Grid, Typography, Tooltip } from "@material-ui/core";
import ContactsIcon from "@material-ui/icons/Contacts";
import Tabs from "./Tabs";
import ClinicalHistory from "../Form/ClinicalHistory";
import TreatmentsHistory from "../Form/TreatmentsHistory";

export default function Profile(props: any) {
  const [isOpen, setOpen] = React.useState(false);

  return (
    <Add
      tooltip="Historial del paciente"
      lightBlue
      handleModal={() => setOpen(!isOpen)}
      open={isOpen}
      icon={<ContactsIcon />}
    >
      <Container maxWidth="md">
        <Grid container>
          <Grid item xs={12}>
            <Tabs
              tabsTitle={[
                <Grid
                  container
                  direction="row"
                  justify="space-between"
                  alignItems="center"
                >
                  <Typography variant="body1">Historial clínico</Typography>
                </Grid>,
                <Grid
                  container
                  direction="row"
                  justify="space-between"
                  alignItems="center"
                >
                  <Typography variant="body1">
                    Historial de tratamientos
                  </Typography>
                </Grid>,
              ]}
              tabsContent={[
                <ClinicalHistory customer_id={props.customer_id as Number} />,
                <TreatmentsHistory patient={props.customer_id as Number} />,
              ]}
            />
          </Grid>
        </Grid>
      </Container>
    </Add>
  );
}
