import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});
interface Props {
  model: String;
  attributes: Array<String>;
  data: Array<Element>;
}
interface Element {
  name: String;
  values: Array<React.ReactNode>;
}
export default function SimpleTable(props: Props) {
  const classes = useStyles();

  return (
    <div style={{ maxWidth: "88vw", overflowX: "auto" }}>
      <TableContainer component={Paper}>
        <Table className={classes.table} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell
                style={{ fontWeight: "bold" }}
                key={"model"}
                align="left"
              >
                {props.model}
              </TableCell>
              {props.attributes.map((attribute: String, index: Number) => (
                <TableCell
                  style={{ fontWeight: "bold" }}
                  key={`cell-${index}`}
                  align="left"
                >
                  {attribute}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {props.data.map((element: any, index: number) => (
              <TableRow
                style={{
                  backgroundColor: index % 2 === 0 ? "var(--gray)" : "inherit",
                }}
                key={"row-" + index}
              >
                <TableCell component="th" scope="row">
                  {element.name}
                </TableCell>
                {element.values.map((value: any, index: Number) => (
                  <TableCell key={index + "-value"} align="left">
                    {value}
                  </TableCell>
                ))}
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  );
}
