import React from "react";
import ICustomer from "../../Classes/Interfaces/ICustomer";
import User from "../../Classes/User";
import {
  Grid,
  TextField,
  Button,
  Avatar,
  CircularProgress,
} from "@material-ui/core";
import { Add } from "@material-ui/icons";
import UserContext from "../../Context/User";

interface State {
  name: String;
  lastName: String;
  results: ICustomer[];
  load: Boolean;
}
interface Props {
  onAddUser: (user: ICustomer) => void;
}
class SearchUser extends React.Component<Props, State> {
  static contextType = UserContext;
  context!: React.ContextType<typeof UserContext>;

  state = {
    name: "",
    lastName: "",
    results: [],
    load: false,
  };

  searchUsers = async () => {
    this.setState({ load: true });
    try {
      const worker = this.context.user as User;
      const response = await worker.searchUsers(
        this.state.name,
        this.state.lastName
      );
      this.setState({
        results: response ? response.map(({ user }: any) => user) : [],
      });
    } catch (error) {
      console.log(error);
    } finally {
      this.setState({ load: false });
    }
  };

  render() {
    return (
      <Grid container spacing={3} direction="row">
        <Grid item sm={6} xs={12}>
          <TextField
            fullWidth
            label="Nombre(s)"
            onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
              this.setState({ name: e.target.value });
            }}
          />
        </Grid>
        <Grid item sm={6} xs={12}>
          <TextField
            fullWidth
            label="Apellidos"
            onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
              this.setState({ lastName: e.target.value });
            }}
          />
        </Grid>
        <Grid item xs={12}>
          <Button
            onClick={this.searchUsers}
            variant="contained"
            color="primary"
          >
            {this.state.load ? (
              <CircularProgress
                style={{
                  height: "1.2rem",
                  width: "1.2rem",
                  fontSize: "1.2rem",
                  color: "white",
                }}
                variant="indeterminate"
              />
            ) : (
              "Buscar"
            )}
          </Button>
        </Grid>
        <Grid item md={12} xs={12}>
          {this.state.results.map((user: ICustomer, index: number) => (
            <Grid
              container
              justify="space-between"
              alignItems="center"
              direction="row"
              style={{ marginBottom: 5 }}
            >
              {`${user.first_name} ${user.last_name}`}
              <Avatar
                style={{
                  height: "1.3rem",
                  width: "1.3rem",
                  backgroundColor: "var(--secondary)",
                  marginLeft: "1.5rem",
                  cursor: "pointer",
                }}
                onClick={() => {
                  this.props.onAddUser(user);
                  const { results } = this.state;
                  results.splice(index, 1);
                  this.setState({ results: results });
                }}
              >
                <Add
                  style={{
                    color: "white",
                    fontSize: "1rem",
                  }}
                />
              </Avatar>
            </Grid>
          ))}
        </Grid>
      </Grid>
    );
  }
}
export default SearchUser;
