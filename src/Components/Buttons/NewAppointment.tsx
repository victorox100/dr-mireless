import React from "react";
import { Avatar, Tooltip } from "@material-ui/core";
import { Event as EventIcon } from "@material-ui/icons";
import { withRouter, RouteComponentProps } from "react-router-dom";

interface Props extends RouteComponentProps {
  userId: Number;
}
function NewAppointment(props: Props) {
  return (
    <Tooltip title="Crear cita">
      <Avatar
        style={{ backgroundColor: "var(--secondary)", cursor: "pointer" }}
        onClick={() => {
          props.history.push(`/citas/${props.userId}`);
        }}
      >
        <EventIcon />
      </Avatar>
    </Tooltip>
  );
}
export default withRouter(NewAppointment);
