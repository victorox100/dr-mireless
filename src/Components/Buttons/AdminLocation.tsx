import React from "react";
import UserContext from "../../Context/User";
import SuperAdmin from "../../Classes/SuperAdmin";
import { Grid, Typography, Tooltip, Avatar, Drawer } from "@material-ui/core";
import ILocation from "../../Classes/Interfaces/ILocation";
import Pagination, { PaginationProps } from "./Pagination";
import { Edit } from "@material-ui/icons";
import { withRouter, RouteComponentProps } from "react-router-dom";

interface State {
  current: ILocation | undefined;
  locations: ILocation[];
  pagination: PaginationProps["pagination"];
  open: Boolean;
}
interface Props extends RouteComponentProps {}

class AdminLocation extends React.Component<Props, State> {
  static contextType = UserContext;
  context!: React.ContextType<typeof UserContext>;
  state = {
    current: undefined,
    locations: [],
    pagination: {
      first: 0,
      next: 0,
      last: 0,
      prev: 0,
    },
    open: false,
  };
  componentDidMount() {
    this.getLocations();
  }

  getLocations = async (page: Number = 1) => {
    const admin: SuperAdmin = this.context.user as SuperAdmin;
    const response: any = await admin.getLocations(page);
    this.setState(
      {
        current:
          response.data.length > 0
            ? (response.data[0] as ILocation)
            : undefined,
        locations: response.data,
        pagination: response.pagination,
      },
      () =>
        admin._setLocation(
          response.data.length > 0 ? (response.data[0] as ILocation).id : 0
        )
    );
  };

  render() {
    return (
      <Grid container direction="row">
        {this.state.locations.length > 0 && this.state.current && (
          <Grid item xs={9} container alignItems="center">
            <Typography style={{ fontSize: "0.895rem" }} variant="body1">
              Administrando la sucursal:{" "}
              {((this.state.current as unknown) as ILocation).shortName}
            </Typography>
          </Grid>
        )}
        <Grid item xs={3}>
          <Tooltip title="Cambiar">
            <Avatar
              onClick={() => this.setState({ open: true })}
              style={{
                marginLeft: 10,
                backgroundColor: "white",
                height: "1rem",
                width: "1rem",
                cursor: "pointer",
              }}
            >
              <Edit
                style={{ fontSize: "0.895rem", color: "var(--principal)" }}
              />
            </Avatar>
          </Tooltip>
        </Grid>
        <Drawer
          open={this.state.open}
          onClose={() => this.setState({ open: false })}
          anchor="top"
        >
          <Grid container>
            {this.state.locations.map((loc: ILocation) => (
              <Grid
                item
                container
                justify="center"
                style={{
                  borderBottomColor: "var(--gray)",
                  borderBottomStyle: "solid",
                  borderBottomWidth: 1,
                  cursor: "pointer",
                }}
                xs={12}
                onClick={() =>
                  this.setState({ current: loc, open: false }, () => {
                    const admin: SuperAdmin = this.context.user as SuperAdmin;
                    admin._setLocation(loc.id);
                    this.props.history.push("/");
                  })
                }
              >
                <Typography style={{ fontSize: "0.895rem" }} variant="body1">
                  {loc.shortName}
                </Typography>
              </Grid>
            ))}
            <Grid item xs={12} container justify="center">
              <Pagination
                getData={this.getLocations}
                pagination={this.state.pagination}
              />
            </Grid>
          </Grid>
        </Drawer>
      </Grid>
    );
  }
}

export default withRouter(AdminLocation);
