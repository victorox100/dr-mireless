import React from "react";
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      "& > *": {
        margin: theme.spacing(1),
      },
    },
    input: {
      display: "none",
    },
  })
);
interface Props {
  name: string;
  accept: string;
  multiple: boolean | undefined;
  text: String;
  style?: React.CSSProperties;
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
}
export default function Upload(props: Props) {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <input
        accept={props.accept}
        className={classes.input}
        id="contained-button-file"
        multiple={props.multiple}
        type="file"
        name={props.name}
        onChange={props.onChange}
      />
      <label htmlFor="contained-button-file">
        <Button
          style={props.style ? { ...props.style } : {}}
          variant="contained"
          color="primary"
          component="span"
        >
          {props.text}
        </Button>
      </label>
    </div>
  );
}
