import React from "react";
import FirstPageIcon from "@material-ui/icons/FirstPage";
import LastPageIcon from "@material-ui/icons/LastPage";
import Previous from "@material-ui/icons/ChevronLeft";
import Next from "@material-ui/icons/ChevronRight";
import Tooltip from "@material-ui/core/Tooltip";

export interface PaginationProps {
  pagination: { first: Number; prev: Number; last: Number; next: Number };
  getData: (page: Number) => void;
}

const Pagination = (props: PaginationProps) => {
  return (
    <>
      {props.pagination.first ? (
        <Tooltip title="Inicio">
          <FirstPageIcon
            style={{ cursor: "pointer" }}
            onClick={() => props.getData(props.pagination.first)}
          />
        </Tooltip>
      ) : null}
      {props.pagination.prev ? (
        <Tooltip title="Anterior">
          <Previous
            style={{ cursor: "pointer" }}
            onClick={() => props.getData(props.pagination.prev)}
          />
        </Tooltip>
      ) : null}
      {props.pagination.next ? (
        <Tooltip title="Siguiente">
          <Next
            style={{ cursor: "pointer" }}
            onClick={() => props.getData(props.pagination.next)}
          />
        </Tooltip>
      ) : null}
      {props.pagination.last ? (
        <Tooltip title="Fin">
          <LastPageIcon
            style={{ cursor: "pointer" }}
            onClick={() => props.getData(props.pagination.last)}
          />
        </Tooltip>
      ) : null}
    </>
  );
};

export default Pagination;
