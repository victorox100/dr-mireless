import React, { useImperativeHandle, useRef, CSSProperties } from "react";
import Avatar from "@material-ui/core/Avatar";
import DeleteIcon from "@material-ui/icons/Delete";
import {
  ConnectDropTarget,
  ConnectDragSource,
  DropTargetMonitor,
  DragSourceMonitor,
} from "react-dnd";
import {
  DragSource,
  DropTarget,
  DropTargetConnector,
  DragSourceConnector,
} from "react-dnd";
import { ItemTypes } from "./ItemTypes";
import { XYCoord } from "dnd-core";
import { Typography } from "@material-ui/core";

const style: CSSProperties = {
  display: "flex",
  flexDirection: "row",
  justifyContent: "space-between",
  border: "1px solid rgba(0, 0, 0, 0.14)",
  borderRadius: 20,
  height: 35,
  alignItems: "center",
  marginBottom: 10,
  cursor: "grab",
};

export interface CardProps {
  id: any;
  text: string;
  index: number;
  moveCard: (dragIndex: number, hoverIndex: number) => void;

  isDragging: boolean;
  connectDragSource: ConnectDragSource;
  connectDropTarget: ConnectDropTarget;
  remove: Function;
}

interface CardInstance {
  getNode(): HTMLDivElement | null;
}

const Card = React.forwardRef<HTMLDivElement, CardProps>(
  (
    { text, index, remove, isDragging, connectDragSource, connectDropTarget },
    ref
  ) => {
    const elementRef = useRef(null);
    connectDragSource(elementRef);
    connectDropTarget(elementRef);

    const opacity = isDragging ? 0 : 1;
    useImperativeHandle<any, CardInstance>(ref, () => ({
      getNode: () => elementRef.current,
    }));
    return (
      <div ref={elementRef} style={{ ...style, opacity }}>
        <Typography
          style={{
            marginRight: 10,
            marginLeft: 10,
          }}
          variant="body1"
        >
          {text}
        </Typography>
        <Avatar
          style={{
            marginRight: 10,
            marginLeft: 10,
            height: "1.2rem",
            width: "1.2rem",
            backgroundColor: "#f44336",
            cursor: "default",
          }}
          onClick={() => {
            remove(index);
          }}
        >
          <DeleteIcon style={{ color: "white", fontSize: "1rem" }} />
        </Avatar>
      </div>
    );
  }
);

export default DropTarget(
  ItemTypes.CARD,
  {
    hover(
      props: CardProps,
      monitor: DropTargetMonitor,
      component: CardInstance
    ) {
      if (!component) {
        return null;
      }
      // node = HTML Div element from imperative API
      const node = component.getNode();
      if (!node) {
        return null;
      }

      const dragIndex = monitor.getItem().index;
      const hoverIndex = props.index;

      // Don't replace items with themselves
      if (dragIndex === hoverIndex) {
        return;
      }

      // Determine rectangle on screen
      const hoverBoundingRect = node.getBoundingClientRect();

      // Get vertical middle
      const hoverMiddleY =
        (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2;

      // Determine mouse position
      const clientOffset = monitor.getClientOffset();

      // Get pixels to the top
      const hoverClientY = (clientOffset as XYCoord).y - hoverBoundingRect.top;

      // Only perform the move when the mouse has crossed half of the items height
      // When dragging downwards, only move when the cursor is below 50%
      // When dragging upwards, only move when the cursor is above 50%

      // Dragging downwards
      if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
        return;
      }

      // Dragging upwards
      if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
        return;
      }

      // Time to actually perform the action
      props.moveCard(dragIndex, hoverIndex);

      // Note: we're mutating the monitor item here!
      // Generally it's better to avoid mutations,
      // but it's good here for the sake of performance
      // to avoid expensive index searches.
      monitor.getItem().index = hoverIndex;
    },
  },
  (connect: DropTargetConnector) => ({
    connectDropTarget: connect.dropTarget(),
  })
)(
  DragSource(
    ItemTypes.CARD,
    {
      beginDrag: (props: CardProps) => ({
        id: props.id,
        index: props.index,
      }),
    },
    (connect: DragSourceConnector, monitor: DragSourceMonitor) => ({
      connectDragSource: connect.dragSource(),
      isDragging: monitor.isDragging(),
    })
  )(Card)
);
