import React from "react";
import Drawer from "@material-ui/core/Drawer";
import FilterIcon from "@material-ui/icons/FilterList";
import { Avatar, Container, Tooltip } from "@material-ui/core";

const Filter = (props: any) => {
  const [isOpen, setOpen] = React.useState<boolean>(false);

  return (
    <>
      <Tooltip title="Filtrar">
        <Avatar
          style={{ color: "white", cursor: "pointer" }}
          onClick={() => setOpen(true)}
        >
          <FilterIcon />
        </Avatar>
      </Tooltip>
      <Drawer open={isOpen} anchor="right" onClose={() => setOpen(false)}>
        <Container maxWidth="xs">{props.children}</Container>
      </Drawer>
    </>
  );
};
export default Filter;
