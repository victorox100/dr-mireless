import React from "react";
import BranchBg from "../Background/BranchBg";

const Loader = () => {
  return (
    <>
      <BranchBg withActivity />
    </>
  );
};
export default Loader;
